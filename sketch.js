var Engine = Matter.Engine,
    World = Matter.World,
    Bodies = Matter.Bodies;

var engine;
var world;
var particles = [];
var plinkos = [];
var rows = 5;
var cols = 5;


function setup() {
    createCanvas(600, 400);
    engine = Engine.create();
    world = engine.world;

    var spacing = width / cols;

    for (var j = 0; j < rows; j++) {
    	for(var i = 0; i < cols; i++ ) {
    		var x =spacing/2+ i * spacing;
    		var y = spacing + j * spacing;
    		var p = new Plinko(x, y, 4)
    		plinkos.push(p);
    	}
    }

}

function draw() {
    if (frameCount % 60 == 0) {
        var p = new Particle(300, 50, 10);
        particles.push(p);
    }
    background(51);
    Engine.update(engine);

    for (var i = 0; i< particles.length; i++){
    	particles[i].show();
    }
    for (var i = 0; i< plinkos.length; i++){
    	plinkos[i].show();
    }
}