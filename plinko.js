function Plinko(x, y, r) {
	var options = {
		isStatic: true
	}
	this.body = Bodies.circle( x, y, r, options);
	World.add(world, this.body);
	this.r = r;
}

Plinko.prototype.show = function() {
	fill(121,22, 63);
	stroke(255);
	var pos = this.body.position;
	push();
	translate(pos.x, pos.y);
	ellipse(0, 0, this.r *2);
	pop();

}